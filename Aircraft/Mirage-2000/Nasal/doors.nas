# =====
# Doors
# =====

crew       = aircraft.door.new("/sim/model/door-positions/crew", 2, 0 );
passenger  = aircraft.door.new("/sim/model/door-positions/passenger", 2, 0 );
temporary  = aircraft.door.new("/sim/model/door-positions/temporary", 2, 0 );
parachute  = aircraft.door.new("/sim/model/door-positions/parachute", 2, 0 )
