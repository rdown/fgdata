<?xml version="1.0"?>
<!--
************************************************************************
Saab J35F

3D model: Oliver Reischl, oliver.reischl@chello.at
FDM, instruments, systems: Anders Martinsson jandersmartinsson@gmail.com
************************************************************************
-->

<PropertyList>

 <sim>

  <description>Saab J35F Draken</description>
  <author>Oliver Reischl (3D model), Anders M (FDM, Cockpit)</author>
  <rating>
    <FDM type="int">3</FDM>
    <systems type="int">2</systems>
    <cockpit type="int">3</cockpit>
    <model type="int">3</model>
  </rating>
  <aircraft-version>0.9.1</aircraft-version> 
  <status>beta</status> 

  <flight-model>jsb</flight-model>
  <aero>DrakenJ35F</aero>

  <startup>
   <splash-texture>Aircraft/SaabJ35F/SaabJ35F-splash.jpg</splash-texture>
  </startup>
  
  <sound>
   <path>Aircraft/SaabJ35F/Sounds/sound.xml</path>
  </sound>

  <virtual-cockpit archive="y">true</virtual-cockpit>
  <allow-toggle-cockpit archive="y">false</allow-toggle-cockpit>
  
  <view n="0">
   <internal archive="y">true</internal>
   <config>
    <pitch-offset-deg>-9</pitch-offset-deg>
    <x-offset-m archive="y">0.0</x-offset-m>
    <y-offset-m archive="y">0.85</y-offset-m>
    <z-offset-m archive="y">-3.42</z-offset-m>
   </config>
  </view>

  <model>
   <path>Aircraft/SaabJ35F/Model/draken.xml</path>
   <livery>
     <file type="string">swe_green_16</file>
  </livery>
  </model>

  <chase-distance-m>-21</chase-distance-m>
  
  <systems>
    <path>Systems/systems.xml</path>
    <autopilot>
      <path>Systems/draken_autopilot.xml</path>
    </autopilot>
  </systems>
<!--
  <ai>
   <scenario>refueling_demo</scenario>
  </ai>
-->

  <!-- turns off GA tower communications -->
  <ATC>
   <enabled type="bool">false</enabled>
  </ATC>

  <!-- turns off GA traffic -->
  <ai-traffic>
   <enabled type="bool">false</enabled>
  </ai-traffic>

  <help>
   <key>
    <name>S</name>
    <desc>Automated engine start</desc>
   </key>
   <key>
    <name>Shift-J</name>
    <desc>Jettison Droptanks in flight</desc>
   </key>
   <key>
    <name>Ctrl-L</name>
    <desc>Attache or remove Droptanks before flight</desc>
   </key>
   <key>
    <name>Ctrl-B</name>
    <desc>Speed brakes extend/retract</desc>
   </key>
   <key>
    <name>Ctrl-D</name>
    <desc>Canopy unlock-open/close-lock</desc>
   </key>
  </help>

 <menubar>
  <default>
   <menu n="100">
    <label>J-35 Draken</label>
    <enabled type="bool">true</enabled>
    <item>
     <label>Select Livery</label>
     <binding>
      <command>nasal</command>
      <script>aircraft.livery.dialog.toggle();</script>
     </binding>
    </item>
   </menu>
  </default>
 </menubar>
 
  <rendering>
    <redout>
      <parameters>
        <blackout-complete-g>9.3</blackout-complete-g>
        <blackout-onset-g>6.5</blackout-onset-g>
      </parameters>
    </redout>
  </rendering>
  
 <flight-recorder>
	<replay-config type="int">0</replay-config>
		<config n="0" include="Systems/draken_recorder.xml">
			<name type="string">Drakens's Flight Recorder</name>
	 </config>
 </flight-recorder>
 
 </sim>

 <limits>
  <vne type="double">729</vne> 
  <max-gear-extended-speed type="double">270</max-gear-extended-speed>
  <max-positive-g type="double">9</max-positive-g>
  <max-negative-g type="double">-4</max-negative-g>
  <max-touch-down-fpm type="double">500</max-touch-down-fpm>
  <max-drop-speed-mach type="double">0.8</max-drop-speed-mach>
 </limits>
 
 <consumables>
 <!-- 0=forward internal, 1 and 2=droptanks L and R, 3=rear internal -->
  <fuel>
   <tank n="0">
    <level-gal_us archive="y">469.0</level-gal_us>
   </tank>
   <tank n="1">
    <level-gal_us archive="y">143.5</level-gal_us>
   </tank>
   <tank n="2">
    <level-gal_us archive="y">143.5</level-gal_us>
   </tank>
   <tank n="3">
    <level-gal_us archive="y">469.0</level-gal_us>
   </tank>
   <using-droptanks type ="bool">false</using-droptanks>
   <pressure-fail type="bool">false</pressure-fail>
  </fuel>
  <droptanks type="bool">true</droptanks>
 </consumables>


  <controls>
    <!-- Start with Parking Brake on -->
     <gear>
        <brake-parking type="double">1.0</brake-parking>
     </gear>
     <canopy>
       <control type="bool">false</control>
     </canopy>
     <electric>
       <battery-switch>false</battery-switch>
     </electric>
  </controls>


 <engines>
  <engine n="0">
    <running>false</running>
  </engine>
 </engines>

<input>
 <keyboard>

   <key n="81">
    <name>Shift-Q</name>
    <desc>Fuel Cutoff Switch or Lever.</desc>
    <binding>
      <condition>
        <property>/sim/input/selected/engine[0]</property>
      </condition>
     <command>property-toggle</command>
     <property>/controls/engines/engine[0]/cutoff</property>
   </binding>
  </key>

  <key n="83">
   <name>Shift-S</name>
   <desc>Autostart systems and engine.</desc>
   <binding>
     <command>nasal</command>
     <script>SaabJ35F.start_systems();</script>
   </binding>
  </key>

  <key n="74">
   <name>Shift-J</name>
   <desc>Drop droptanks.</desc>
   <binding>
     <command>nasal</command>
     <script>SaabJ35F.drophandle(1);</script>
   </binding>
  </key>

  <key n="12">
   <name>Ctrl-L</name>
   <desc>Drop droptanks.</desc>
   <binding>
     <command>nasal</command>
     <script>SaabJ35F.drophandle(0);</script>
   </binding>
  </key>
  
	<key n="4">
		<name>Ctrl-d</name>
	        <desc>Toggle Canopy</desc>
	          <binding> 
	          	<command>nasal</command>
			<script>SaabJ35F.canopy_operate();</script>
		  </binding>
	</key>

 <key n="103">
  <name>g</name>
  <desc>Gear Up</desc>
  <binding>
   <command>nasal</command>
   <script><![CDATA[
      if (getprop("controls/engines/engine[0]/throttle") > 0.9) controls.gearDown(-1);
   ]]></script>
  </binding>
  <mod-up>
   <binding>
    <command>nasal</command>
    <script>controls.gearDown(0)</script>
   </binding>
  </mod-up>
 </key>
 	
 </keyboard>
</input>

<!-- Target in meters -->
<autopilot>
  <enabled type="bool">true</enabled>
  <on type="bool">false</on>
  <settings>
     <target-altitude-m type="double">0</target-altitude-m>
  </settings>
  <locks>
    <altitude type="bool">false</altitude>
    <attitude type="bool">false</attitude>
    <damp type="bool">true</damp>
  </locks>
</autopilot>

           
<instrumentation>
 <AHK>
  <mode type="int">0</mode>
  <needle_target type="double">0</needle_target>
  <needle_dist type="double">0</needle_dist>
 </AHK>
 <navradio>
  <mode type="int" archive="y">0</mode> 
  <anita1 type="int" archive="y">0</anita1>
  <anita2 type="int" archive="y">0</anita2>
  <barbro type="int" archive="y">0</barbro>
  <dir type="double">0</dir>
  <dis type="double">0</dis>
  <gs_alt type="double">0</gs_alt>
  <gs_dir type="double">0</gs_dir>
 </navradio>  
 <radar>
  <mode type="int" archive="y">0</mode>
  <scan_mode type="float">120</scan_mode>
  <antenna_pitch type="float">0</antenna_pitch>
  <antenna_yaw type="float">0</antenna_yaw>
 </radar>
 <gear_warning type="bool">false</gear_warning>
 <alt_indicator type="bool">false</alt_indicator>
 <master_alarm>
   <on type="bool">false</on>
   <enabled type="bool">true</enabled>
 </master_alarm>
 <fuel>
  <needleF_rot type="double">0</needleF_rot>
  <needleB_rot type="double">0</needleB_rot>
  <FT_light type="bool">true</FT_light>
  <LT_light type="bool">true</LT_light>
 </fuel>
 <switches>
  <drop_selector>
   <pos type="bool">false</pos>
  </drop_selector>
  <battery_cover>
   <pos type="bool">false</pos>
  </battery_cover>
  <fuel_cover>
   <pos type="bool">false</pos>
  </fuel_cover>
  <fuel>
   <pos type="bool">false</pos>
  </fuel>
  <generator>
   <pos type="int">0</pos>
  </generator>
  <start>
   <pos type="bool">false</pos>
  </start>
  <restart>
   <pos type="bool">false</pos>
  </restart>
  <inst-light-knob>
   <pos type="double" archive="y">1</pos>
  </inst-light-knob>
  <default>
   <pos type="bool">false</pos>
  </default>
 </switches>
 <hydraulic>
   <hydI-fail type="bool">true</hydI-fail>
   <hydII-fail type="bool">true</hydII-fail>
 </hydraulic>
</instrumentation>

<nasal>
 <SaabJ35F>
  <file>Nasal/SaabJ35F.nas</file>
  <file>Nasal/draken_limits.nas</file>
  <file>Nasal/caution_panel.nas</file> 
  <file>Nasal/draken_AHK.nas</file>
  <file>Nasal/draken_nav.nas</file>
  <file>Nasal/systemp.nas</file>
  <file>Radar/radar.nas</file>
 </SaabJ35F>
</nasal>

</PropertyList>
